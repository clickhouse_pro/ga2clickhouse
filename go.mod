module bitbucket.org/clickhouse_pro/ga2clickhouse

go 1.13

require (
	bitbucket.org/clickhouse_pro/components v0.0.0-20180208070017-8c55b42c9de7
	github.com/digitalcrab/browscap_go v0.0.0-20160912072603-465055751e36
	github.com/gocraft/web v0.0.0-20190207150652-9707327fb69b
	github.com/oschwald/maxminddb-golang v1.5.0
	github.com/roistat/go-clickhouse v1.0.1
	github.com/rs/zerolog v1.15.0
	github.com/sebest/xff v0.0.0-20160910043805-6c115e0ffa35
	github.com/stretchr/testify v1.4.0 // indirect
)
