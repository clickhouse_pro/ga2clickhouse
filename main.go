package main

import (
	"bitbucket.org/clickhouse_pro/components/browscap"
	"bitbucket.org/clickhouse_pro/ga2clickhouse/ga2clickhouse"
)

func main() {
	collector := ga2clickhouse.NewGA2ClickHouse()
	defer collector.ShutdownCollector()
	collector.ParseCmdlineArgs()
	collector.CreateDataDir()
	collector.InitGeoIP()
	browscap.InitBrowsCap()
	collector.InitClickHouseClient()
	collector.CreateClickHouseTables()
	collector.SpawnProcessRequest()
	collector.InitRouterAndListWebServer()
}
