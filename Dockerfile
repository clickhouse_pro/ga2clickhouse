FROM golang:alpine AS builder
MAINTAINER Eugene Klimov <bloodjazman@gmail.com>

WORKDIR /ga2clickhouse/
VOLUME /ga2clickhouse/data

ENV GOPATH /ga2clickhouse/

COPY ./.gometalinter.json /ga2clickhouse/
COPY ./ga2clickhouse/ /ga2clickhouse/src/bitbucket.org/clickhouse_pro/ga2clickhouse/ga2clickhouse/
COPY ./cmd/ /ga2clickhouse/src/bitbucket.org/clickhouse_pro/ga2clickhouse/cmd/
COPY ./id_rsa /root/.ssh/id_rsa
RUN chmod 0600 /root/.ssh/id_rsa

RUN apk add --no-cache --update git openssh-client
RUN touch /root/.ssh/known_hosts
RUN ssh-keygen -R github.com
RUN ssh-keygen -R bitbucket.org
RUN ssh-keyscan -H github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts
RUN git config --global url."git@github.com:".insteadOf "https://github.com/"
RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"


RUN mkdir -m 0755 -p /var/lib/GeoIP/
ADD http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz /var/lib/GeoIP/GeoLite2-City.mmdb.gz
RUN gzip -d /var/lib/GeoIP/GeoLite2-City.mmdb.gz


RUN mkdir -p /ga2clickhouse/bin \
    && cd /ga2clickhouse/ \
    && go get -v ./src/bitbucket.org/clickhouse_pro/ga2clickhouse/ga2clickhouse/...

RUN go get -u github.com/alecthomas/gometalinter
RUN /ga2clickhouse/bin/gometalinter --install
RUN grep -r InitBrowsCap /ga2clickhouse/src
RUN /ga2clickhouse/bin/gometalinter --config=/ga2clickhouse/.gometalinter.json --exclude=/go/src/ /ga2clickhouse/src/bitbucket.org/clickhouse_pro/ga2clickhouse/...

RUN go test -v bitbucket.org/clickhouse_pro/ga2clickhouse/ga2clickhouse \
    && go build -o /ga2clickhouse/bin/ga2clickhouse /ga2clickhouse/src/bitbucket.org/clickhouse_pro/ga2clickhouse/main.go \
    && rm -rf /ga2clickhouse/src


FROM alpine:latest
ENV DOCKERIZE_VERSION v0.4.0
RUN apk add --no-cache --update ca-certificates openssl git \
		&& apk add --no-cache --update gzip sed \
    && update-ca-certificates \
    && wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY --from=builder /ga2clickhouse/bin/ga2clickhouse /ga2clickhouse/bin/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY --from=builder /var/lib/GeoIP/GeoLite2-City.mmdb /var/lib/GeoIP/GeoLite2-City.mmdb
RUN echo "wget -O - http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz | gzip -d > /var/lib/GeoIP/GeoLite2-City.mmdb" > /etc/periodic/weekly/geoiplite2

ENTRYPOINT ["/bin/sh","-c"]