package ga2clickhouse

import (
	"github.com/gocraft/web"
	"net/http"
	"net/url"
)

func newTestRequest() web.Request {
	req := web.Request{
		Request: &http.Request{},
	}
	req.Form = url.Values{
		"cd1": []string{"test"},
		"cm2": []string{"10"},

		"il3nm": []string{"ListName"},

		"il4pi5id":   []string{"ListProductId4"},
		"il5pi5nm":   []string{"ListProductName5"},
		"il6pi6br":   []string{"ListProductBrand6"},
		"il7pi7ca":   []string{"ListProductVariant7"},
		"il8pi8va":   []string{"ListProductCategory8"},
		"il9pi9pr":   []string{"19.99"},
		"il10pi10qt": []string{"100"},
		"il11pi11cc": []string{"ListProductCouponCode11"},
		"il12pi12ps": []string{"12"},

		"pr13id": []string{"ProductId13"},
		"pr14nm": []string{"ProductName14"},
		"pr15br": []string{"ProductBrand15"},
		"pr16va": []string{"ProductVariant16"},
		"pr17ca": []string{"ProductCategory17"},
		"pr18pr": []string{"18.99"},
		"pr19qt": []string{"19"},
		"pr20cc": []string{"ListProductCouponCode20"},
		"pr21ps": []string{"21"},

		"pr22cd1": []string{"ProductDimension1"},
		"pr23cd2": []string{"ProductDimension2"},

		"il24pi22cd22": []string{"ProductDimension1"},
		"il25pi23cd23": []string{"ProductDimension2"},

		"pr26cm1": []string{"1"},
		"pr27cm5": []string{"5"},

		"il28pi24cm24": []string{"24"},
		"il29pi25cm25": []string{"25"},

		"promo30id": []string{"PromoID"},
		"promo31nm": []string{"PromoName"},
		"promo32cr": []string{"PromoCreative"},
		"promo33ps": []string{"10"},
	}
	return req
}
