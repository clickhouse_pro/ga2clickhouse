package ga2clickhouse

import (
	"net/url"
	"os"
	"sync"

	"github.com/oschwald/maxminddb-golang"
	"github.com/roistat/go-clickhouse"
)

// GA2ClickHouse main collector structure
// @TODO 4h/DEV maybe it will bad idea use one global struct with sync primitives, think about it
type GA2ClickHouse struct {
	dataDir         string
	listen          string
	trackID         string
	database        string
	tablePrefix     string
	clusterName     string
	dbFieldList     []string
	formKExistsMap  map[string]bool
	dbFieldMap      map[string]int
	dbToFormMap     map[string]string
	nestedTables    map[string]nestedTableType
	bufferedData    chan url.Values
	clickhouse      *clickhouse.Cluster
	clickhouseConns []*clickhouse.Conn
	clickhouseHosts []string
	csvFileName     string
	csvFile         *os.File
	geoipDir        string
	geoip           *maxminddb.Reader
	wg              *sync.WaitGroup
	mx              *sync.RWMutex
	writedRows      uint
	chunkSize       uint
	chunkTimeout    uint
	schemaVersion   uint16
	dropDatabase    bool
}

type nestedTableType map[string]tablePrefixes

type tablePrefixes struct {
	prefixes       []string
	formKExistsMap map[string]bool
	// int it's a position in db_fields_list
	indexDbKeyMap map[string]int
	valueDbKeyMap map[string]int
	// map where field value possible exists in form
	valuesDbToFormMap map[string]map[string]bool
}

// GAFieldsMap see https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters
var GAFieldsMap = map[string]string{
	// user identificators
	"cid": "clientId",
	"uid": "userId",
	"tid": "trackerId",
	// basic parameters
	"v":  "version",
	"t":  "type",
	"dr": "referer",

	"cn": "utm_campaign",
	"cs": "utm_source",
	"cm": "utm_medium",
	"ck": "utm_keyword",
	"cc": "utm_content",

	"gclid":  "googleClickId",
	"dclid":  "googleMediaId",
	"linkid": "linkId",

	"an":   "applicationName",
	"av":   "applicationVersion",
	"aid":  "applicationId",
	"aiid": "applicationInstallerId",

	"sr": "screenResolution",
	"vp": "screenViewPort",
	"sd": "screenColors",
	"de": "encoding",
	"ul": "language",
	"je": "javaEnabled",
	"fl": "flashVersion",
	"ua": "userAgent",
	"dt": "title",
	"cd": "screenName",
	"dh": "hostName",
	"dl": "pageLocation",
	"dp": "pagePath",
	"ec": "eventCategory",
	"ea": "eventAction",
	"el": "eventLabel",
	"ev": "eventValue",

	"ti":  "transactionId",
	"ta":  "transactionAffiliation",
	"tr":  "transactionRevenue",
	"ts":  "transactionShipping",
	"tt":  "transactionTax",
	"tcc": "transactionCoupon",
	"cu":  "currency",

	"in": "itemName",
	"ip": "itemPrice",
	"iq": "itemQuantity",
	"ic": "itemSku",
	"iv": "itemCategory",

	// advanced parameters
	"ds":  "dataSource",
	"qt":  "queryTime",
	"sc":  "seanseControl",
	"uip": "ip",
	//see https://developers.google.com/analytics/devguides/collection/protocol/v1/geoid
	"geoid": "geoid",

	/*
		https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce
		https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#enhanced-ecomm
	*/
	"pa":     "enhancedEcommerceAction",
	"pal":    "enhancedEcommerceActionList",
	"cos":    "enhancedEcommerceCheckoutStep",
	"col":    "enhancedEcommerceCheckoutVariant",
	"promoa": "promoAction",

	/* https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#social */
	"sn": "socialNetwork",
	"sa": "socialAction",
	"st": "socialTarget",

	/* https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#timing */
	"utc": "userTimingCategory",
	"utv": "userTimingAction",
	"utt": "userTimingTime",
	"utl": "userTimingLabel",
	"plt": "pageLoadTime",
	"dns": "dnsTime",
	"pdt": "pageDownloadTime",
	"rrt": "redirectTime",
	"tcp": "tcpTime",
	"srt": "serverResponseTime",
	"dit": "domInteractionTime",
	"clt": "contentLoadTime",

	// https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#exception
	"exd": "exceptionData",
	"exf": "exceptionFatal",

	//https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#experiments
	"xid":  "experimentId",
	"xvar": "experimentVariant",

	//collected time
	"time": "timestamp",

	// userAgent not GA compatible fields
	"ua_name":  "browserName",
	"ua_v":     "browserVersion",
	"os_name":  "os",
	"os_v":     "osVersion",
	"device":   "device",
	"device_t": "deviceType",

	// geoIp not GA compatible fields
	"geo_latt":    "geoLattitude",
	"geo_long":    "geoLongitude",
	"geo_region":  "geoRegion",
	"geo_country": "geoCountry",
	"geo_city":    "geoCity",
	"anonym":      "isAnonymous",
	//	"geo_asnum":    "geoASNUM",
	//	"geo_provider": "geoProvider",

}

var dbFieldTypes = map[string]string{
	"enhancedEcommerce.id":              "String",
	"enhancedEcommerce.name":            "String",
	"enhancedEcommerce.brand":           "String",
	"enhancedEcommerce.category":        "String",
	"enhancedEcommerce.variant":         "String",
	"enhancedEcommerce.price":           "Float32",
	"enhancedEcommerce.coupon_code":     "String",
	"enhancedEcommerceDimensions.value": "String",
	"enhancedEcommerce.quantity":        "Int16",
	"enhancedEcommerce.position":        "Int16",
	"enhancedEcommerceMetrics.value":    "Int64",
	"customMetrics.value":               "Float64",
}

var createTableTemplate = `CREATE TABLE IF NOT EXISTS %s (
		timestamp UInt32,
		date MATERIALIZED toDate(timestamp),

		type String,
		version String,
		trackerId String,

		clientId String,
		userId String,

		ip String,
		geoLattitude Float32,
		geoLongitude Float32,
		geoRegion String,
		geoCountry String,
		geoCity String,
		isAnonymous UInt8,

		screenResolution String,
		screenViewPort String,
		screenColors String,
		encoding String,
		language String,
		javaEnabled UInt8,
		flashVersion String,

		userAgent String,
		browserName String,
		browserVersion String,
		os String,
		osVersion String,
		device String,
		deviceType String,

		title String,
		screenName String,
		pageLocation String,
		pagePath String,
		hostName String,

		referer String,

		utm_campaign String,
		utm_source String,
		utm_medium String,
		utm_keyword String,
		utm_content String,

		googleClickId String,
		googleMediaId String,
		linkId String,

		applicationName String,
		applicationId String,
		applicationVersion String,
		applicationInstallerId String,

		eventCategory String,
		eventAction String,
		eventLabel String,
		eventValue Int64,

		transactionId String,
		transactionRevenue Float32,
		transactionTax Float32,
		transactionShipping Float32,
		transactionAffiliation String,

		itemName String,
		itemCategory String,
		itemSku String,
		itemQuantity Int16,
		itemPrice Float32,

		currency FixedString(3),

		dataSource String,
		queryTime Int32,
		seanseControl String,

		customDimensions Nested (
			index UInt16,
			value String
		),
		customMetrics Nested (
			index UInt16,
			value Float64
		),
		/*
		https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce
		https://developers.google.com/analytics/devguides/collection/protocol/v1/parameters#enhanced-ecomm
		*/
		enhancedEcommerceAction String,
		enhancedEcommerceActionList String,
		enhancedEcommerceCheckoutStep UInt16,
		enhancedEcommerceCheckoutVariant UInt16,

		enhancedEcommerceList Nested (
			listIndex UInt16,
			listName String
		),

		enhancedEcommerce Nested (
			listIndex UInt16,
			productIndex UInt16,
			id String,
			name String,
			brand String,
			category String,
			variant String,
			price Float32,
			quantity Int16,
			coupon_code String,
			position Int16
		),

		enhancedEcommerceDimensions Nested (
			listIndex UInt16,
			productIndex UInt16,
			dimensionIndex UInt16,
			value String
		),

		enhancedEcommerceMetrics Nested (
			listIndex UInt16,
			productIndex UInt16,
			metricIndex UInt16,
			value Int64
		),

		promoAction String,
		promo Nested (
			index UInt16,
			id String,
			name String,
			creative String,
			position String
		),

		socialNetwork String,
		socialAction String,
		socialTarget String,

		userTimingCategory String,
		userTimingAction String,
		userTimingTime UInt32,
		userTimingLabel String,
		pageLoadTime UInt32,
		dnsTime UInt32,
		pageDownloadTime UInt32,
		redirectTime UInt32,
		tcpTime UInt32,
		serverResponseTime UInt32,
		domInteractionTime UInt32,
		contentLoadTime UInt32,

		exceptionData String,
		exceptionFatal String,

		experimentId String,
		experimentVariant String

) %s`
