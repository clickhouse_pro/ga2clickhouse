package ga2clickhouse

import (
	"fmt"
	"html"
	"strings"

	"github.com/rs/zerolog/log"

	"github.com/gocraft/web"
)

var pixelGIF = []byte{71, 73, 70, 56, 57, 97, 1, 0, 1, 0, 144, 0, 0, 255, 0, 0, 0, 0, 0, 33, 249, 4, 5, 16, 0, 0, 0, 44, 0, 0, 0, 0, 1, 0, 1, 0, 0, 2, 2, 4, 1, 0, 59}

func (g *GA2ClickHouse) collectHit(rw web.ResponseWriter, req *web.Request) {
	rw.Header().Set("Access-Control-Allow-Origin", "*")
	rw.Header().Set("Content-Type", "image/gif")
	err := req.ParseForm()
	if err == nil {
		parseTimestamp(req)
		parseUserAgent(req)
		parseGeoIP(req, g)
		parseUTM(req)
		g.bufferedData <- req.Form
	}
	_, err = rw.Write(pixelGIF)
	if err != nil {
		log.Print(err)
	}
}

func (g *GA2ClickHouse) showCounter(rw web.ResponseWriter, req *web.Request) {
	rw.Header().Set("Content-Type", "text/javascript")
	fmt.Fprint(rw, `
		function Ga2ClickHousePlugin(tracker) {
			var ga = window[window['GoogleAnalyticsObject'] || 'ga'];
			if (typeof ga == 'function') {
				ga(function(tracker) {
					var originalSendHitTask = tracker.get('sendHitTask');
					tracker.set('sendHitTask', function(model) {
						var payLoad = model.get('hitPayload');
						var collectRequest = new XMLHttpRequest();
						var collectPath = "//`+req.Host+`/ga2clickhouse.collect";
						collectRequest.open('POST', collectPath, true);
						collectRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
						collectRequest.send(payLoad);
						originalSendHitTask(model);
					});
				});
			}
		}

		function providePlugin(pluginName, pluginConstructor) {
			var ga = window[window['GoogleAnalyticsObject'] || 'ga'];
			if (typeof ga == 'function') {
				ga('provide', pluginName, pluginConstructor);
			} else {
				console.log('ga is undefined plase read https://developers.google.com/analytics/devguides/collection/analyticsjs/');
			}
		}
		providePlugin('ga2clickhouse', Ga2ClickHousePlugin);
	`)
}

func (g *GA2ClickHouse) showExample(rw web.ResponseWriter, req *web.Request) {
	rw.Header().Set("Content-Type", "text/html; charset=utf-8")
	counterCode := `
<html>
<head>
 <title>ga2clickhouse example</title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
 <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
 <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', '` + g.trackID + `', 'auto');

  </script>
  <script async src="//` + req.Host + `/ga2clickhouse.js"></script>
</head>
<body bgcolor="#FFFFFF">
<script>
  ga('require', 'ec');
  ga('require', 'ga2clickhouse');
  ga('set', '&cu', 'EUR');

  ga('ec:addImpression', {
	  'id': 'ga2clickhouse',
	  'name': 'ga2clickhouse collector',
	  'category': 'Software/Collectors',
	  'brand': 'clickhouse.pro',
	  'variant': 'free edition',
	  'list': 'View Products',
	  'position': 1,
	  'dimension1': 'Registered User',
	  'metric1': 10
  });

   ga('ec:addImpression', {
	  'name': 'snowplow2clickhouse collector',
	  'category': 'Software/Collectors',
	  'brand': 'clickhouse.pro',
	  'variant': 'free edition',
	  'list': 'View Products',
	  'position': 1,
	  'dimension1': 'Anonymous user'
  });


  ga('ec:addProduct', {
	  'id': 'ga2clickhouse',
	  'variant': 'free edition',
	  'price': '9.99',
	  'quantity': 1
  });

  ga('ec:setAction', 'purchase', {
	  'id': 'T1',
	  'affiliation': 'Cloudpayments - subscribe',
	  'revenue': '9.99',
	  'tax': '0.25',
  });

  ga('ec:setAction', 'purchase', {
	  'id': 'T2',
	  'affiliation': 'Cloudpayments - subscribe with deployemnt',
	  'revenue': '9.99',
	  'tax': '2.5',
	  'shipping': '99.99'
  });

  ga('send', 'pageview');
</script>
<h1>Your first ga2clickhouse example</h1>
{{snippet}}
</body>
</html>
	`

	counterCode = strings.Replace(
		counterCode,
		"{{snippet}}",
		"<pre class=\"prettyprint\"><code class=\"language-html\">"+strings.Replace(html.EscapeString(counterCode), g.trackID, "UA-XXXXX-X", 1)+"</code></pre>",
		1,
	)

	fmt.Fprint(rw, counterCode)
}
