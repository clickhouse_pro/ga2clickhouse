package ga2clickhouse

import (
	"net"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/rs/zerolog/log"

	"os"

	"github.com/digitalcrab/browscap_go"
	"github.com/gocraft/web"
	"github.com/sebest/xff"
)

// NewGA2ClickHouse create new GA to Clickhouse streaming structure
func NewGA2ClickHouse() *GA2ClickHouse {
	return &GA2ClickHouse{
		schemaVersion:  1,
		formKExistsMap: make(map[string]bool),
		dbFieldList:    make([]string, 0),
		dbFieldMap:     make(map[string]int),
		dbToFormMap:    make(map[string]string),
		nestedTables:   make(map[string]nestedTableType),

		bufferedData: make(chan url.Values, 100),

		mx: &sync.RWMutex{},
		wg: &sync.WaitGroup{},
	}
}

// (prefix([0-9]+))(suffix1([0-9]*))?(suffix1([0-9]*))? regexp match
// nolint: gocyclo
func isPrefixAndSuffixWithNumbers(prefixes []string, s string) bool {
	var sliceBegin, sliceEnd int
	l := len(prefixes)

	for i, v := range prefixes {
		prefixLen := len(v)
		if sliceBegin+prefixLen > len(s) || s[sliceBegin:sliceBegin+prefixLen] != v {
			return false
		}
		sliceBegin += prefixLen

		if i < l-1 {
			vNext := prefixes[i+1]
			vNextBegin := strings.Index(s[sliceBegin:], vNext)
			if vNextBegin == -1 || vNextBegin == 0 {
				return false
			}
			sliceEnd = sliceBegin + vNextBegin
		} else {
			sliceEnd = len(s)
		}
		if i == 0 && s[sliceBegin:sliceEnd] == "" {
			return false
		}
		for _, c := range s[sliceBegin:sliceEnd] {
			if c < 48 || c > 57 {
				return false
			}
		}
		sliceBegin = sliceEnd
	}
	return true
}

// extractPrefixValuesFromComplexField (prefix([0-9]+))(suffix1([0-9]*))?(suffix1([0-9]*))? extract number indexes to strings
// nolint: gocyclo
func extractPrefixValuesFromComplexField(prefixes []string, s string) []string {
	var sliceBegin, sliceEnd int
	l := len(prefixes)

	indexes := make([]string, l)

	for i, v := range prefixes {
		prefixLen := len(v)
		if sliceBegin+prefixLen > len(s) || s[sliceBegin:sliceBegin+prefixLen] != v {
			return indexes
		}
		sliceBegin += prefixLen

		if i < l-1 {
			vNext := prefixes[i+1]
			vNextBegin := strings.Index(s[sliceBegin:], vNext)
			if vNextBegin == -1 || vNextBegin == 0 {
				return indexes
			}
			sliceEnd = sliceBegin + vNextBegin
		} else {
			sliceEnd = len(s)
		}
		if i == 0 && s[sliceBegin:sliceEnd] == "" {
			return indexes
		}
		for _, c := range s[sliceBegin:sliceEnd] {
			if c < 48 || c > 57 {
				return indexes
			}
		}
		indexes[i] = s[sliceBegin:sliceEnd]
		sliceBegin = sliceEnd
	}
	return indexes
}

func parseUserAgent(req *web.Request) {

	var ua *browscap_go.Browser
	var ok bool
	if _, uaExists := req.Form["ua"]; uaExists {
		ua, ok = browscap_go.GetBrowser(req.Form.Get("ua"))
	} else {
		ua, ok = browscap_go.GetBrowser(req.UserAgent())
		req.Form.Set("ua", req.UserAgent())
	}

	if ok {
		req.Form.Set("ua_name", ua.Browser)
		req.Form.Set("ua_v", ua.BrowserVersion)
		req.Form.Set("os_name", ua.Platform)
		req.Form.Set("os_v", ua.PlatformVersion)
		req.Form.Set("device", ua.DeviceName)
		req.Form.Set("device_t", ua.DeviceType)
	}
}

func parseGeoIP(req *web.Request, g *GA2ClickHouse) interface{} {
	ip := req.Form.Get("uip")
	if ip == "" {
		if req.Header.Get("X-Forwarded-For") != "" {
			ip = xff.Parse(req.Header.Get("X-Forwarded-For"))
		} else if req.Header.Get("X-Real-IP") != "" {
			ip = req.Header.Get("X-Real-IP")
		} else {
			ip = req.RemoteAddr
		}
	}
	parsedIP := net.ParseIP(ip)
	if parsedIP != nil {
		var geoipLookup struct {
			Country struct {
				IsoCode string `maxminddb:"iso_code"`
			} `maxminddb:"country"`
			City struct {
				Names map[string]string `maxminddb:"names"`
			} `maxminddb:"city"`
			Continent struct {
				Names map[string]string `maxminddb:"names"`
			} `maxminddb:"continent"`
			Location struct {
				Latitude  float64 `maxminddb:"latitude"`
				Longitude float64 `maxminddb:"longitude"`
			} `maxminddb:"location"`
			Traits struct {
				IsAnonymousProxy bool `maxminddb:"is_anonymous_proxy"`
			} `maxminddb:"traits"`
		}

		if err := g.geoip.Lookup(parsedIP, &geoipLookup); err == nil {
			if geoipLookup.Country.IsoCode != "" {
				req.Form.Set("geo_latt", strconv.FormatFloat(geoipLookup.Location.Latitude, 'f', 6, 64))
				req.Form.Set("geo_long", strconv.FormatFloat(geoipLookup.Location.Longitude, 'f', 6, 64))
				req.Form.Set("geo_country", geoipLookup.Country.IsoCode)
				req.Form.Set("geo_city", geoipLookup.City.Names["en"])
				//@TODO 10m/DEV add fields "geo_asnum", "geo_provider",
				if geoipLookup.Traits.IsAnonymousProxy {
					req.Form.Set("anonym", "1")
				} else {
					req.Form.Set("anonym", "0")
				}
			} else {
				return nil
			}
		}
		return geoipLookup
	}
	return nil
}

func parseTimestamp(req *web.Request) {
	req.Form.Set("time", strconv.FormatInt(time.Now().Unix(), 10))
}

func parseUTM(req *web.Request) {
	refString := req.Referer()
	if refString != "" {
		ref, parseURLErr := url.Parse(refString)
		if parseURLErr == nil && ref.RawQuery != "" {
			q, queryErr := url.ParseQuery(ref.RawQuery)
			if parseURLErr == nil {
				for _, k := range [5]string{"cn", "cs", "cm", "ck", "cc"} {
					if req.Form.Get(k) == "" && q.Get(GAFieldsMap[k]) != "" {
						req.Form.Set(k, q.Get(GAFieldsMap[k]))
					}
				}
			} else {
				log.Error().Err(queryErr).Msg("parse referer query string error")
			}
		} else if parseURLErr != nil {
			log.Error().Err(parseURLErr).Msg("parse referer url error")
		}
	}
}

func parseNestedField(prefixes []string, k string, nestedFields []string, g *GA2ClickHouse) bool {
	if isPrefixAndSuffixWithNumbers(prefixes, k) {
		prefixesK := strings.Join(prefixes, ",")
		g.mx.Lock()
		defer g.mx.Unlock()
		g.formKExistsMap[k] = true
		for _, f := range nestedFields {
			if _, fExists := g.dbFieldMap[f]; !fExists {
				g.dbFieldList = append(g.dbFieldList, f)
				g.dbFieldMap[f] = len(g.dbFieldList) - 1
			}
			t := f[:strings.Index(f, ".")]
			nestedTable, tExists := g.nestedTables[t]
			if !tExists {
				nestedTable = nestedTableType{}
			}

			prefixStruct, pExists := nestedTable[prefixesK]
			if !pExists {
				prefixStruct = tablePrefixes{
					prefixes:          prefixes,
					formKExistsMap:    make(map[string]bool),
					indexDbKeyMap:     make(map[string]int),
					valueDbKeyMap:     make(map[string]int),
					valuesDbToFormMap: make(map[string]map[string]bool),
				}
			}
			prefixStruct.formKExistsMap[k] = true
			if strings.Contains(f, "index") || strings.Contains(f, "Index") {
				prefixStruct.indexDbKeyMap[f] = g.dbFieldMap[f]
			} else {
				prefixStruct.valueDbKeyMap[f] = g.dbFieldMap[f]
				valuesDbToFormMap, vExists := prefixStruct.valuesDbToFormMap[f]
				if !vExists {
					valuesDbToFormMap = make(map[string]bool)
				}
				valuesDbToFormMap[k] = true
				prefixStruct.valuesDbToFormMap[f] = valuesDbToFormMap
			}

			nestedTable[prefixesK] = prefixStruct
			g.nestedTables[t] = nestedTable
		}
		return true
	}
	return false
}

func formatToDbType(s string, dbType string) string {
	switch dbType {
	case "String":
		s = "'" + s + "'"
	case "Float32":
		s = formatFloat(s, 32, 4)
	case "Float64":
		s = formatFloat(s, 64, 4)
	case "Int8", "Int16", "Int32", "Int64":
		bits, err := strconv.Atoi(dbType[3:])
		if err != nil {
			s = "0"
		} else {
			s = formatInt(s, bits, true)
		}
	case "UInt8", "UInt16", "UInt32", "UInt64":
		bits, err := strconv.Atoi(dbType[4:])
		if err != nil {
			s = "0"
		} else {
			s = formatInt(s, bits, false)
		}
	}
	return s
}
func formatFloat(s string, bytes int, precisoin int) string {
	f, err := strconv.ParseFloat(s, bytes)
	if err != nil {
		f = 0.0
	}
	s = strconv.FormatFloat(f, 'f', precisoin, bytes)
	return s
}
func formatInt(s string, bits int, sign bool) string {
	i, err := strconv.ParseInt(s, 10, bits)
	if err != nil {
		i = 0
	}
	if !sign && i < 0 {
		i = -i
	}
	s = strconv.FormatInt(i, 10)
	return s
}
func isPrefixValuesExists(checkedValues []string, existsValues [][]string) bool {
	for _, prefixValues := range existsValues {
		if isSliceEqual(checkedValues, prefixValues) {
			return true
		}
	}
	return false
}

func isSliceEqual(a []string, b []string) bool {
	if a == nil && b == nil {
		return true
	}

	if a == nil || b == nil {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}

// nolint: gocyclo
func extractIndexField(dbField string, formKey string, dbComplex []string, prefixValues []string) []string {
	if strings.Contains(dbField, "listIndex") && formKey[:2] == "pr" {
		dbComplex = append(dbComplex, "0")
	} else if strings.Contains(dbField, "listIndex") && formKey[:2] == "il" {
		dbComplex = append(dbComplex, prefixValues[0])
	} else if strings.Contains(dbField, "productIndex") && formKey[:2] == "pr" {
		dbComplex = append(dbComplex, prefixValues[0])
	} else if strings.Contains(dbField, "productIndex") && formKey[:2] == "il" {
		dbComplex = append(dbComplex, prefixValues[1])
	} else if strings.Contains(dbField, "Index") && formKey[:2] == "pr" {
		dbComplex = append(dbComplex, prefixValues[1])
	} else if strings.Contains(dbField, "Index") && formKey[:2] == "il" {
		dbComplex = append(dbComplex, prefixValues[2])
	} else if strings.Contains(dbField, "index") {
		dbComplex = append(dbComplex, prefixValues[0])
	}
	return dbComplex
}

func createDirIfNotExists(dirName string, mode os.FileMode) {
	if _, err := os.Stat(dirName); os.IsNotExist(err) {
		if err = os.Mkdir(dirName, mode); err != nil {
			log.Fatal().Err(err).Str("dirName", dirName).Uint32("mode", uint32(mode)).Msgf("error create directory")
		}
	}
}
