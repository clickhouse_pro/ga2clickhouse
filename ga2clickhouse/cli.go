package ga2clickhouse

import (
	"flag"
	"fmt"
	stdlog "log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"bitbucket.org/clickhouse_pro/components/gocraft_stacktrace"

	"github.com/gocraft/web"
	"github.com/oschwald/maxminddb-golang"
	"github.com/roistat/go-clickhouse"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// ParseCmdlineArgs Parsing command line args
func (g *GA2ClickHouse) ParseCmdlineArgs() {
	var clickhouseHosts string
	var debug bool
	var console bool

	flag.CommandLine = flag.NewFlagSet(os.Args[0], flag.PanicOnError)
	// @TODO 2h/DEV need integrate to Zookeeper API for grab all click house hosts from current cluster
	flag.BoolVar(&debug, "debug", false, "Set log level to debug")
	flag.BoolVar(&console, "console", false, "Output log into pretty console format")
	flag.StringVar(&clickhouseHosts, "clickhouseHosts", "localhost:8123", "Comma separated clickhouse hosts")
	flag.StringVar(&g.dataDir, "dataDir", "./data", "Directory for data processing")
	flag.StringVar(&g.listen, "listen", ":8321", "Listen directive")
	flag.BoolVar(&g.dropDatabase, "dropDatabase", false, "Drop and create database")
	flag.StringVar(&g.database, "database", "ga2clickhouse", "Clickhouse database name")
	flag.StringVar(&g.tablePrefix, "tablePrefix", "", "Clickhouse tables prefix")
	flag.StringVar(&g.clusterName, "clusterName", "ga2clickhouse", "Clickhouse cluster name for distributed tables")
	flag.StringVar(&g.trackID, "trackID", "UA-85417423-1", "Google Analytics Tracking ID see https://support.google.com/analytics/answer/1008080")
	flag.StringVar(&g.geoipDir, "geoipDir", "/var/lib/GeoIP/", "Directory with maxmind geoip database files")
	flag.UintVar(&g.chunkSize, "chunkSize", 10000, "Flush data to clickhouse every X requests")
	// @TODO 2h/DEV implement chunkTimeout
	flag.UintVar(&g.chunkTimeout, "chunkTimeout", 60, "Flush data to clickhouse every X seconds, @TODO Not Implemented yet")
	flag.Parse()
	g.clickhouseHosts = make([]string, strings.Count(clickhouseHosts, ",")+1)
	g.clickhouseHosts = strings.Split(clickhouseHosts, ",")

	stdlog.SetOutput(log.Logger)
	if debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	if console {
		log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	}
	geoipDir, err := filepath.Abs(g.geoipDir)
	if err != nil {
		log.Fatal().Err(err).Str("geoipDir", g.geoipDir).Msg("wrong path")
	}
	if _, err := os.Stat(geoipDir); os.IsNotExist(err) {
		log.Fatal().Err(err).Str("geoipDir", geoipDir).Msg("path not exists")
	}
	g.geoipDir = geoipDir
	log.Print("Command Line parameters parsed")
}

// InitClickHouseClient make slice of clickhouse.Conn from -clickhouseHosts params
func (g *GA2ClickHouse) InitClickHouseClient() {
	transport := clickhouse.NewHttpTransport()
	g.clickhouseConns = make([]*clickhouse.Conn, len(g.clickhouseHosts))
	for i, host := range g.clickhouseHosts {
		g.clickhouseConns[i] = clickhouse.NewConn(host, transport)
	}

	g.clickhouse = clickhouse.NewCluster(g.clickhouseConns...)
	g.clickhouse.OnCheckError(func(c *clickhouse.Conn) {
		log.Printf("Clickhouse connection failed %s\n", c.Host)
	})
	go func() {
		for {
			g.clickhouse.Check()
			time.Sleep(time.Minute)
		}
	}()
	log.Print("ClickHouse Cluster Initialized successful")
}

// CreateClickHouseTables create tables if not exists see createTableTemplate, use ClickHouse HTTP protocol
func (g *GA2ClickHouse) CreateClickHouseTables() {
	queries := make([]string, 0)
	if g.dropDatabase {
		queries = append(queries, "DROP DATABASE IF EXISTS "+g.database)
	}
	createQueries := []string{
		"CREATE DATABASE IF NOT EXISTS " + g.database,
		fmt.Sprintf(
			createTableTemplate,
			g.database+"."+g.tablePrefix+"hits_local",
			"ENGINE = MergeTree(date, (trackerId, date), 8192)",
		),
		fmt.Sprintf(
			createTableTemplate,
			g.database+"."+g.tablePrefix+"hits_distributed",
			"ENGINE = Distributed("+g.clusterName+", "+g.database+", "+g.tablePrefix+"hits_local)",
		),
	}
	queries = append(queries, createQueries...)

	q := clickhouse.NewQuery("")
	lenCon := len(g.clickhouseConns)
	lenQ := len(queries)

	for j := 0; j < lenQ; j++ {
		for i := 0; i < lenCon; i++ {
			q.Stmt = queries[j]
			if err := q.Exec(g.clickhouseConns[i]); err != nil {
				stacktrace.DumpErrorStackAndExit(err)
			}
		}
	}

	log.Printf(
		"Create ClickHouse cluster='%s' database='%s' tablePrefix='%s' dropDatabase=%t COMPLETE",
		g.clusterName, g.database, g.tablePrefix, g.dropDatabase,
	)
}

// CreateDataDir create data directory for temporary CSV files
// nolint: gas
func (g *GA2ClickHouse) CreateDataDir() {
	createDirIfNotExists(g.dataDir, 0755)
	createDirIfNotExists(g.dataDir+"/process", 0755)
	createDirIfNotExists(g.dataDir+"/collect", 0755)
	createDirIfNotExists(g.dataDir+"/archive", 0755)
	createDirIfNotExists(g.dataDir+"/errors", 0755)
	log.Info().Msg("Data dicretories created")
}

// InitGeoIP use MaxMind GeoLite2 database for geo enrichment
func (g *GA2ClickHouse) InitGeoIP() {
	file := g.geoipDir + "/GeoLite2-City.mmdb"
	geoip, err := maxminddb.Open(file)
	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	} else {
		log.Info().Msgf("GEOIP %s opened", file)
	}
	g.geoip = geoip
}

//InitRouterAndListWebServer gocraft/web initialization
func (g *GA2ClickHouse) InitRouterAndListWebServer() {
	router := web.New(*g).
		Middleware(web.LoggerMiddleware).
		Middleware(gocraft_stacktrace.LogErrorMiddleware).
		Get("/ga2clickhouse.collect", g.collectHit).
		Post("/ga2clickhouse.collect", g.collectHit).
		Get("/ga2clickhouse.js", g.showCounter).
		Post("/ga2clickhouse.js", g.showCounter).
		Get("/analytics_js_example.html", g.showExample)

	log.Printf("InitRouterAndListWebServer BEGIN listen=%s", g.listen)
	if err := http.ListenAndServe(g.listen, router); err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	log.Info().Msg("done InitRouterAndListWebServer")
}

//ShutdownCollector exit function
func (g *GA2ClickHouse) ShutdownCollector() {
	log.Info().Msgf("ShutdownCollector BEGIN g.writedRows=%d", g.writedRows)
	close(g.bufferedData)
	g.wg.Wait()

	if g.writedRows > 0 {
		g.mx.Lock()
		g.pushDataToProcessDir()
		g.mx.Unlock()
		g.wg.Wait()
	}

	if err := g.geoip.Close(); err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}

	log.Info().Msg("ShutdownCollector END")
}
