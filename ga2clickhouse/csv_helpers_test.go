package ga2clickhouse

import (
	"strings"
	"testing"
)

type ExpectedNestedDbField struct {
	Fields    []string
	NestedLen int
}

func TestRefreshCSVShemaWithComplexKey(t *testing.T) {
	g := NewGA2ClickHouse()
	req := newTestRequest()
	expectedNestedTable := map[string]ExpectedNestedDbField{
		"customDimensions": {
			Fields:    []string{"index", "value"},
			NestedLen: 1,
		},
		"customMetrics": {
			Fields:    []string{"index", "value"},
			NestedLen: 1,
		},
		"enhancedEcommerceList": {
			NestedLen: 1,
			Fields:    []string{"listIndex", "listName"},
		},

		"enhancedEcommerce": {
			NestedLen: 18,
			Fields: []string{
				"listIndex", "productIndex",
				"id", "name", "brand", "category", "variant", "price", "quantity", "coupon_code", "position",
			},
		},

		"enhancedEcommerceDimensions": {
			NestedLen: 4,
			Fields:    []string{"listIndex", "productIndex", "dimensionIndex", "value"},
		},

		"enhancedEcommerceMetrics": {
			NestedLen: 4,
			Fields:    []string{"listIndex", "productIndex", "metricIndex", "value"},
		},

		"promo": {
			NestedLen: 4,
			Fields:    []string{"index", "id", "name", "creative", "position"},
		},
	}

	for k := range req.Form {
		refreshCSVShemaWithComplexKey(g, k)
	}
	row := g.prepareCSVRow(req.Form)
	if len(row) != len(g.dbFieldList) {
		t.Fatalf("len(row) %d != %d len(g.dbFieldList)", len(row), len(g.dbFieldList))
	}
	if len(req.Form) != len(g.formKExistsMap) {
		t.Fatalf("len(req.Form) %d != %d len(g.formKExistsMap)", len(req.Form), len(g.formKExistsMap))
	}

	for tableName, expected := range expectedNestedTable {
		for _, expectedField := range expected.Fields {
			found := false
			for i, dbField := range g.dbFieldList {
				if dbField == tableName+"."+expectedField {
					found = true
					actualLen := strings.Count(row[i], ",")
					if actualLen != expected.NestedLen-1 {
						t.Fatalf("%s expected %d elements got %d row[%d]=%s", dbField, expected.NestedLen-1, actualLen, i, row[i])
					}
				}
			}
			if !found {
				t.Fatalf("%s not found", tableName+"."+expectedField)
			}
		}
	}
}
