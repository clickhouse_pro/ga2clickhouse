package ga2clickhouse

import (
	"bytes"
	"fmt"
	"net/url"
	"os"
	"strings"
	"time"

	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"github.com/rs/zerolog/log"
)

func refreshCSVShemaWithFormKey(g *GA2ClickHouse, k string) bool {
	g.mx.Lock()
	defer g.mx.Unlock()

	g.formKExistsMap[k] = true
	g.dbFieldList = append(g.dbFieldList, GAFieldsMap[k])
	g.dbToFormMap[GAFieldsMap[k]] = k
	return true
}

// nolint: gocyclo
func refreshCSVShemaWithComplexKey(g *GA2ClickHouse, k string) bool {
	if parseNestedField(
		[]string{"cd"}, k, []string{"customDimensions.index", "customDimensions.value"}, g,
	) {
		return true
	}

	if parseNestedField(
		[]string{"cm"}, k, []string{"customMetrics.index", "customMetrics.value"}, g,
	) {
		return true
	}

	if parseNestedField(
		[]string{"il", "nm"},
		k,
		[]string{
			"enhancedEcommerceList.listIndex",
			"enhancedEcommerceList.listName",
		},
		g,
	) {
		return true
	}

	for suffix, suffixField := range map[string]string{
		"id": "id", "nm": "name", "br": "brand", "va": "variant", "ca": "category",
		"pr": "price", "qt": "quantity", "cc": "coupon_code", "ps": "position",
	} {

		if parseNestedField(
			[]string{"il", "pi", suffix}, k,
			[]string{
				"enhancedEcommerce.listIndex",
				"enhancedEcommerce.productIndex",
				"enhancedEcommerce." + suffixField,
			},
			g,
		) {
			return true
		}

		if parseNestedField(
			[]string{"pr", suffix}, k,
			[]string{
				"enhancedEcommerce.listIndex",
				"enhancedEcommerce.productIndex",
				"enhancedEcommerce." + suffixField},
			g,
		) {
			return true
		}
	}

	for suffix, definition := range map[string]map[string]string{
		"cd": {"table": "enhancedEcommerceDimensions", "dim_field": "dimensionIndex"},
		"cm": {"table": "enhancedEcommerceMetrics", "dim_field": "metricIndex"},
	} {
		if parseNestedField(
			[]string{"pr", suffix},
			k,
			[]string{
				definition["table"] + ".listIndex",
				definition["table"] + ".productIndex",
				definition["table"] + "." + definition["dim_field"],
				definition["table"] + ".value",
			}, g,
		) {
			return true
		}

		if parseNestedField(
			[]string{"il", "pi", suffix},
			k,
			[]string{
				definition["table"] + ".listIndex",
				definition["table"] + ".productIndex",
				definition["table"] + "." + definition["dim_field"],
				definition["table"] + ".value",
			}, g,
		) {
			return true
		}

	}

	for suffix, suffixField := range map[string]string{"id": "id", "nm": "name", "cr": "creative", "ps": "position"} {
		if parseNestedField(
			[]string{"promo", suffix}, k, []string{"promo.index", "promo." + suffixField}, g,
		) {
			return true
		}
	}
	return false
}

//nolint: gas
func (g *GA2ClickHouse) initCSVWriter() {
	g.csvFileName = fmt.Sprintf(
		"%s/collect/%s.%d.csv", g.dataDir, time.Now().UTC().Format("2006-01-02__15-04-05.999999999"), g.schemaVersion,
	)

	csvFile, err := os.OpenFile(g.csvFileName, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0644)
	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	g.csvFile = csvFile
}

//nolint: gas
func (g *GA2ClickHouse) writeCSVHead() {
	head := fmt.Sprintf(
		"INSERT INTO %s.%shits_local (%s) FORMAT CSV\n", g.database, g.tablePrefix, strings.Join(g.dbFieldList, ","),
	)
	if _, err := g.csvFile.WriteString(head); err != nil {
		log.Fatal().Err(err).Msg("writeCSVHead error")
	}
}

// nolint: gocyclo
func (g *GA2ClickHouse) writeCSVRow(form url.Values) {
	var err error
	row := g.prepareCSVRow(form)

	const doubleQuote uint8 = 34
	var bDoubleQoute = []byte{doubleQuote}
	var bDoubleQouteQuoted = []byte{doubleQuote, doubleQuote}

	const singleQuote uint8 = 39
	var bSingleQuote = []byte{singleQuote}

	const commaASCII uint8 = 44
	var bComma = []byte{commaASCII}

	const newLine uint8 = 10
	var bNewLine = []byte{newLine}

	const endLine uint8 = 13
	var bEndLine = []byte{endLine}

	for i, str := range row {
		b := []byte(str)
		hasQuote := bytes.Contains(b, bDoubleQoute) || bytes.Contains(b, bSingleQuote)
		hasComma := bytes.Contains(b, bComma)
		hasNewline := bytes.Contains(b, bNewLine) || bytes.Contains(b, bEndLine)
		if hasQuote {
			b = bytes.Replace(b, bDoubleQoute, bDoubleQouteQuoted, -1)
		}
		if hasComma || hasQuote || hasNewline {
			b = append(bDoubleQoute, append(b, doubleQuote)...)
		}
		_, err = g.csvFile.Write(b)
		if err == nil && i < len(row)-1 {
			_, err = g.csvFile.Write(bComma)
		}
		if err != nil {
			break
		}
	}

	if err == nil {
		_, err = g.csvFile.Write(bNewLine)
	}

	if err != nil {
		log.Error().Err(err).Msg("writeCSVRow error")
	}

	g.writedRows++
}

func (g *GA2ClickHouse) prepareCSVRow(form url.Values) []string {
	dbLen := len(g.dbFieldList)
	row := make([]string, dbLen)
	rowComplex := make(map[string][]string)
	for i := 0; i < dbLen; i++ {
		dbField := g.dbFieldList[i]
		if formKey, formExists := g.dbToFormMap[dbField]; formExists {
			row[i] = form.Get(formKey)
		}
	}

	for _, nestedTable := range g.nestedTables {
		existsPrefixValues := make(map[string][][]string)
		allPrefixNames := make([]string, len(nestedTable))
		i := 0
		for prefixName := range nestedTable {
			allPrefixNames[i] = prefixName
			i++
		}
		existsPrefixValues, rowComplex = prepareNestedIndexFields(nestedTable, form, allPrefixNames, existsPrefixValues, rowComplex)
		rowComplex = prepareNestedValueFields(nestedTable, rowComplex, existsPrefixValues, form)
	}
	for i := 0; i < dbLen; i++ {
		dbField := g.dbFieldList[i]
		complexValues, complexExists := rowComplex[dbField]
		if complexExists {
			row[i] = "[" + strings.Join(complexValues, ",") + "]"
		}
	}
	return row
}
func prepareNestedIndexFields(nestedTable nestedTableType, form url.Values, allPrefixNames []string, existsPrefixValues map[string][][]string, rowComplex map[string][]string) (map[string][][]string, map[string][]string) {
	for _, prefixStruct := range nestedTable {
		for formKey := range prefixStruct.formKExistsMap {
			if formValue := form.Get(formKey); formValue != "" {
				prefixValues := extractPrefixValuesFromComplexField(prefixStruct.prefixes, formKey)
				if prefixValues[0] != "" {
					existsPrefixValues = prepareAllExistsPrefixValues(allPrefixNames, nestedTable, prefixValues, existsPrefixValues)

					for dbField := range prefixStruct.indexDbKeyMap {
						if _, exists := rowComplex[dbField]; !exists {
							rowComplex[dbField] = make([]string, 0)
						}
						rowComplex[dbField] = extractIndexField(dbField, formKey, rowComplex[dbField], prefixValues)
					}
				}
			}
		}
	}
	return existsPrefixValues, rowComplex
}
func prepareNestedValueFields(nestedTable nestedTableType, rowComplex map[string][]string, existsPrefixValues map[string][][]string, form url.Values) map[string][]string {
	for prefixNames, prefixStruct := range nestedTable {
		for dbField := range prefixStruct.valueDbKeyMap {
			if _, exists := rowComplex[dbField]; !exists {
				rowComplex[dbField] = make([]string, 0)
			}
			dbComplex := rowComplex[dbField]
			l := len(prefixStruct.prefixes)
			for _, prefixValues := range existsPrefixValues[prefixNames] {
				formKey := ""
				for i := 0; i < l; i++ {
					formKey += prefixStruct.prefixes[i] + prefixValues[i]
				}
				formValue := ""
				if _, fExists := prefixStruct.valuesDbToFormMap[dbField][formKey]; fExists {
					formValue = form.Get(formKey)
				}

				if dbType, exists := dbFieldTypes[dbField]; exists {
					dbComplex = append(dbComplex, formatToDbType(formValue, dbType))
				} else {
					dbComplex = append(dbComplex, "'"+formValue+"'")
				}

			}
			rowComplex[dbField] = dbComplex
		}
	}
	return rowComplex
}
func prepareAllExistsPrefixValues(allPrefixNames []string, nestedTable nestedTableType, prefixValues []string, existsPrefixValues map[string][][]string) map[string][][]string {
	for i := 0; i < len(allPrefixNames); i++ {
		if len(nestedTable[allPrefixNames[i]].prefixes) == len(prefixValues) {
			if _, pExists := existsPrefixValues[allPrefixNames[i]]; !pExists {
				existsPrefixValues[allPrefixNames[i]] = make([][]string, 0)
			}
			if !isPrefixValuesExists(prefixValues, existsPrefixValues[allPrefixNames[i]]) {
				existsPrefixValues[allPrefixNames[i]] = append(existsPrefixValues[allPrefixNames[i]], prefixValues)
			}
		}
	}
	return existsPrefixValues
}
