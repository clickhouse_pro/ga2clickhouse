package ga2clickhouse

import (
	"net/http"
	"net/url"
	"testing"

	"github.com/gocraft/web"
)

func TestParseGeoip(t *testing.T) {
	g := NewGA2ClickHouse()
	// used system GeoIP lib via https://packages.ubuntu.com/geoipupdate
	g.geoipDir = "/var/lib/GeoIP"
	g.InitGeoIP()
	req := web.Request{
		Request: &http.Request{},
	}
	req.Form = url.Values{}

	for _, ip := range []string{"80.249.80.187", "2001:470:0:76::2", "2001:470:1f1d:275::7"} {
		req.RemoteAddr = ip
		if parseGeoIP(&req, g) == nil {
			t.Errorf("%s not found", ip)
		}
	}
	for _, ip := range []string{"", "10.10.10", "127.0.0.1", "2001:", "192.168.1.1"} {
		req.RemoteAddr = ip
		lookup := parseGeoIP(&req, g)
		if lookup != nil {
			t.Errorf("%s wrong found %v", ip, lookup)
		}
	}
	g.geoip.Close()
}

type expectedNestedPrefixes struct {
	formFields map[string]bool
	prefixes   []string
}

func TestParseNestedField(t *testing.T) {
	g := NewGA2ClickHouse()
	req := newTestRequest()
	expectedResults := []expectedNestedPrefixes{
		{prefixes: []string{"cd"}, formFields: map[string]bool{"cd1": true}},
		{prefixes: []string{"cm"}, formFields: map[string]bool{"cm2": true}},
		{prefixes: []string{"il", "nm"}, formFields: map[string]bool{"il3nm": true}},
		{prefixes: []string{"il", "pi", "id"}, formFields: map[string]bool{"il4pi5id": true}},

		{prefixes: []string{"il", "pi", "nm"}, formFields: map[string]bool{"il5pi5nm": true}},
		{prefixes: []string{"il", "pi", "br"}, formFields: map[string]bool{"il6pi6br": true}},
		{prefixes: []string{"il", "pi", "ca"}, formFields: map[string]bool{"il7pi7ca": true}},
		{prefixes: []string{"il", "pi", "va"}, formFields: map[string]bool{"il8pi8va": true}},
		{prefixes: []string{"il", "pi", "pr"}, formFields: map[string]bool{"il9pi9pr": true}},
		{prefixes: []string{"il", "pi", "qt"}, formFields: map[string]bool{"il10pi10qt": true}},
		{prefixes: []string{"il", "pi", "cc"}, formFields: map[string]bool{"il11pi11cc": true}},
		{prefixes: []string{"il", "pi", "ps"}, formFields: map[string]bool{"il12pi12ps": true}},

		{prefixes: []string{"pr", "id"}, formFields: map[string]bool{"pr13id": true}},
		{prefixes: []string{"pr", "nm"}, formFields: map[string]bool{"pr14nm": true}},
		{prefixes: []string{"pr", "br"}, formFields: map[string]bool{"pr15br": true}},
		{prefixes: []string{"pr", "va"}, formFields: map[string]bool{"pr16va": true}},
		{prefixes: []string{"pr", "ca"}, formFields: map[string]bool{"pr17ca": true}},
		{prefixes: []string{"pr", "pr"}, formFields: map[string]bool{"pr18pr": true}},
		{prefixes: []string{"pr", "qt"}, formFields: map[string]bool{"pr19qt": true}},
		{prefixes: []string{"pr", "cc"}, formFields: map[string]bool{"pr20cc": true}},
		{prefixes: []string{"pr", "ps"}, formFields: map[string]bool{"pr21ps": true}},

		{prefixes: []string{"pr", "cd"}, formFields: map[string]bool{"pr22cd1": true, "pr23cd2": true}},
		{prefixes: []string{"il", "pi", "cd"}, formFields: map[string]bool{"il24pi22cd22": true, "il25pi23cd23": true}},

		{prefixes: []string{"pr", "cm"}, formFields: map[string]bool{"pr26cm1": true, "pr27cm5": true}},
		{prefixes: []string{"il", "pi", "cm"}, formFields: map[string]bool{"il28pi24cm24": true, "il29pi25cm25": true}},

		{prefixes: []string{"promo", "id"}, formFields: map[string]bool{"promo30id": true}},
		{prefixes: []string{"promo", "nm"}, formFields: map[string]bool{"promo31nm": true}},
		{prefixes: []string{"promo", "cr"}, formFields: map[string]bool{"promo32cr": true}},
		{prefixes: []string{"promo", "ps"}, formFields: map[string]bool{"promo33ps": true}},
	}
	for k := range req.Form {
		for _, expected := range expectedResults {
			res := parseNestedField(expected.prefixes, k, make([]string, 0), g)
			if expectedResult, isExists := expected.formFields[k]; isExists {
				if expectedResult != res {
					t.Fatalf("prefixes=%v k=%s expectedResult=%t != res=%t", expected.prefixes, k, expectedResult, res)
				}
			} else {
				if res {
					t.Fatalf("prefixes=%v k=%s expectedResult=%t != res=%t", expected.prefixes, k, false, res)
				}
			}
		}

	}
}
func TestFormatToDbType(t *testing.T) {
	if formatToDbType("", "Int16") != "0" {
		t.Fatalf("formatToDbType(\"\",\"Int16\")=%s\n", formatToDbType("", "Int16"))
	}
}
