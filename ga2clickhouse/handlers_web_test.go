package ga2clickhouse

import (
	"net/http"
	"testing"

	"bitbucket.org/clickhouse_pro/components/browscap"
	"github.com/gocraft/web"
	"github.com/rs/zerolog/log"
)

type testResponseWriter struct {
	web.ResponseWriter
}

func (w testResponseWriter) Write(data []byte) (n int, err error) {
	return len(data), nil
}

func (w testResponseWriter) Header() http.Header {
	return http.Header{}
}

func prepareCollectorToBenchmark() *GA2ClickHouse {
	g := NewGA2ClickHouse()
	g.dropDatabase = true
	g.chunkSize = 1000
	g.dataDir = "/tmp/data"
	g.geoipDir = "/var/lib/GeoIP"
	g.database = "ga2clickhouse_test"
	g.clusterName = "ga2clickhouse_test"
	g.tablePrefix = ""
	g.clickhouseHosts = []string{"local.ga2clickhouse.clickhouse.pro:8123"}
	g.CreateDataDir()
	g.InitGeoIP()
	browscap.InitBrowsCap()
	g.InitClickHouseClient()
	g.CreateClickHouseTables()
	g.SpawnProcessRequest()
	return g
}

func BenchmarkGA2ClickHouse_CollectSequenced(b *testing.B) {
	g := prepareCollectorToBenchmark()
	defer g.ShutdownCollector()

	log.Printf("SEQUENCED b.N=%d", b.N)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		req := newTestRequest()
		rw := web.ResponseWriter(testResponseWriter{})
		g.collectHit(rw, &req)
	}
}

func BenchmarkGA2ClickHouse_CollectParallel(b *testing.B) {
	g := prepareCollectorToBenchmark()
	defer g.ShutdownCollector()

	log.Printf("PARALLEL b.N=%d", b.N)
	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		rw := web.ResponseWriter(testResponseWriter{})

		for pb.Next() {
			req := newTestRequest()
			g.collectHit(rw, &req)
		}
	})
}
