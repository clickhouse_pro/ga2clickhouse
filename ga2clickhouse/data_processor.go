package ga2clickhouse

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"errors"
	"net/http"
	"net/url"
	"os"
	"strings"

	"bitbucket.org/clickhouse_pro/components/stacktrace"
	"github.com/rs/zerolog/log"

	"fmt"

	"github.com/roistat/go-clickhouse"
)

//SpawnProcessRequest run goroutine request receiver via channel
func (g *GA2ClickHouse) SpawnProcessRequest() {
	log.Print("SpawnProcessRequest BEGIN")
	g.wg.Add(1)
	go g.processRequest()
	log.Print("SpawnProcessRequest END")
}

func (g *GA2ClickHouse) processRequest() {
	log.Print("processRequest BEGIN")
	defer g.wg.Done()
	for form := range g.bufferedData {
		refreshSchema := false
		g.mx.Lock()
		refreshSchema = g.processFormData(form, refreshSchema)
		if refreshSchema {
			if g.writedRows > 0 {
				g.pushDataToProcessDir()
				g.schemaVersion++
			}
			g.initCSVWriter()
			g.writeCSVHead()
		}

		g.writeCSVRow(form)

		if g.writedRows > 0 && (g.writedRows%g.chunkSize) == 0 {
			g.pushDataToProcessDir()
			g.initCSVWriter()
			g.writeCSVHead()
		}
		g.mx.Unlock()

	}
	log.Info().Msg("processRequest END after channel closed")
}
func (g *GA2ClickHouse) processFormData(form url.Values, refreshSchema bool) bool {
	for k := range form {
		_, fExists := g.formKExistsMap[k]
		_, gaExists := GAFieldsMap[k]

		if !fExists && gaExists {
			refreshSchema = refreshCSVShemaWithFormKey(g, k) || refreshSchema
		}

		if !fExists && !gaExists {
			refreshSchema = refreshCSVShemaWithComplexKey(g, k) || refreshSchema
		}
	}
	return refreshSchema
}

func (g *GA2ClickHouse) pushDataToProcessDir() {
	g.flushAllDataAndCloseDesciptors()
	processCsvFileName := strings.Replace(g.csvFileName, "/collect/", "/process/", 1)
	if err := os.Rename(g.csvFileName, processCsvFileName); err == nil {
		log.Printf("%s -> %s \n", g.csvFile.Name(), processCsvFileName)
		g.writedRows = 0
		if !g.clickhouse.IsDown() {
			g.wg.Add(1)
			go g.batchInsertCsvToClickHouse(processCsvFileName)
		} else {
			stacktrace.DumpErrorStackAndExit(errors.New("clickhouse cluster is down"))
		}
	} else {
		stacktrace.DumpErrorStackAndExit(err)
	}
}

func (g *GA2ClickHouse) flushAllDataAndCloseDesciptors() {
	log.Info().Msg("flushAllDataAndCloseDesciptors BEGIN")

	if err := g.csvFile.Close(); err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	log.Info().Msg("flushAllDataAndCloseDesciptors END")
}

// TODO refactoring to use zlib compression handler
func (g *GA2ClickHouse) batchInsertCsvToClickHouse(csvFileName string) {
	defer g.wg.Done()
	log.Printf("batchInsertCsvToClickHouse BEGIN csvFileName=%s", csvFileName)

	conn := g.clickhouse.ActiveConn()
	if conn == nil {
		log.Info().Msg("Currently no live ClickHouse Connection")
		return
	}

	err := g.uploadCsvStreamToClickHouse(csvFileName, conn)

	if err != nil {
		log.Printf("Error csvFileName=%s %s", csvFileName, err.Error())
		return
	}
	archiveCsvFileName := strings.Replace(csvFileName, "/process/", "/archive/", 1)
	err = os.Rename(csvFileName, archiveCsvFileName)
	if err == nil {
		err = gzipCsvFile(archiveCsvFileName)
		if err == nil {
			err = os.Remove(archiveCsvFileName)
		}
	}
	if err != nil {
		stacktrace.DumpErrorStackAndExit(err)
	}
	log.Printf("%s -> %s.gz", csvFileName, archiveCsvFileName)
	log.Print("batchInsertCsvToClickHouse END")
}
func (g *GA2ClickHouse) uploadCsvStreamToClickHouse(csvFileName string, conn *clickhouse.Conn) error {
	var resp *http.Response
	var req *http.Request

	csvFile, err := os.Open(csvFileName)
	if err == nil {
		req, err = http.NewRequest("POST", conn.Host, csvFile)
		if err == nil {
			req.Header.Set("Content-Type", "text/plain")
			client := http.Client{}
			resp, err = client.Do(req)
			if err == nil {
				if resp.StatusCode != http.StatusOK {
					buf := bytes.Buffer{}
					_, err = buf.ReadFrom(resp.Body)
					if err == nil {
						stacktrace.DumpErrorStackAndExit(fmt.Errorf("csvFileName=%v resp.StatusCode=%v responseBody=%v", csvFileName, resp.StatusCode, buf.String()))
					} else {
						stacktrace.DumpErrorStackAndExit(fmt.Errorf("csvFileName=%v resp.StatusCode=%v responseBodyReadError=%v", csvFileName, resp.StatusCode, err))
					}
				}
				err = resp.Body.Close()
			}
		}
	}
	return err
}

func gzipCsvFile(archiveCsvFileName string) error {
	var err error
	var archiveCsvFile, archiveGzFile *os.File
	var readedBytes int
	var info os.FileInfo
	const kb8 = 1024 * 8

	archiveCsvFile, err = os.Open(archiveCsvFileName)
	if err == nil {
		archiveGzFile, err = os.Create(archiveCsvFileName + ".gz")
		if err == nil {
			info, err = archiveCsvFile.Stat()
			if err == nil {
				chunkBytes := make([]byte, kb8)
				buffer := bufio.NewReader(archiveCsvFile)
				size := info.Size()
				var i int64
				gzWriter := gzip.NewWriter(archiveGzFile)
				for i = 0; i < size; i += kb8 {
					readedBytes, err = buffer.Read(chunkBytes)
					if err == nil {
						_, err = gzWriter.Write(chunkBytes[:readedBytes])
					}

					if err != nil {
						break
					}
				}
				if err = gzWriter.Close(); err != nil {
					return err
				}
			}
		}

	}

	_ = archiveCsvFile.Close()
	_ = archiveGzFile.Close()
	return err
}
